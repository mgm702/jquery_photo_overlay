jQuery_photo_overlay
====================

Basic jQuery project that displays images in overlay format for cleaner presentation

The images are royalty free pictures taken from the internet and I claim no right to them. They are used simply for displaying the jQuery function in multiple scenarios.

----Clicking on any of the images found on image_page.html will create a larger overlay image with the caption below the picture. This is a nice presentation trick used to focus in large retina pictures. 
